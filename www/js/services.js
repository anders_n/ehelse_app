/**
 * Created by LarsKristian on 18.08.2015.
 */
'use strict';

/* Services */
var myAppServices = angular.module('fhirApp.services', []);

myAppServices.service('BLE', function ($rootScope, Settings) {

    var BLE = this;
    var myDevice;
    var devices = [];

    var sensorValues = {};
    BLE.getSensorValues = function() {
        return sensorValues;
    };

    var sensorMessage = {};
    BLE.getSensorMessage = function() {
        return sensorMessage;
    };

    BLE.getDevices = function () {
        return devices;
    };

    BLE.scan = function () {
        console.log('Scanning for devices...');
        easyble.stopScan();
        easyble.closeConnectedDevices();
        BLE.startScan();
    };

    BLE.disconnect = function () {
        console.log("Scanning stopped");
        easyble.closeConnectedDevices();
        easyble.stopScan();
        if (myDevice != undefined) {
            myDevice.close();
        }
    };

    BLE.reset = function () {
        evothings.ble.reset();
    };

    BLE.startScan = function () {
        easyble.startScan(function (device) {
            console.log('Found device: ' + JSON.stringify(device));
            var arrayLength = devices.length;
            var deviceAlreadyThere = false;
            for (var i = 0; i < arrayLength; i++) {
                if (devices[i].address == device.address) {
                    deviceAlreadyThere = true;
                }
            }
            if (!deviceAlreadyThere) {
                console.log('Adding device: ' + device.name);
                if (typeof device.name === 'undefined' || device.name === null) {
                    device.number = device.address;
                } else {
                    device.number = device.name;
                }
                $rootScope.$apply(function () {
                    devices.push(device);
                });
            } else {
                console.log('Already have device: ' + device.name + ' in ' + JSON.stringify(devices));
            }

            if (device.address == Settings.settings.pulseOximetryDevice) { //'00:1C:05:FF:E1:16') {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                sensorValues.deviceType = 'SpO2';
                BLE.connect(Settings.settings.pulseOximetryDevice);
            } else if (device.address == Settings.settings.scaleDevice) { //'5C:31:3E:01:18:A9') {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                sensorValues.deviceType = 'Scale';
                BLE.connect(Settings.settings.scaleDevice);
            } if (device.address == Settings.settings.bpDevice) { //'5C:31:3E:01:81:48') {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                BLE.connect(Settings.settings.bpDevice);
                sensorValues.deviceType = 'BP';
            } if (device.address == Settings.settings.pulseBelt ) { //'5C:31:3E:01:81:48') {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                sensorValues.deviceType = 'HRM';
                BLE.connect(Settings.settings.pulseBelt);
            } if (device.address == Settings.settings.glucose ) {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Connecting to ' + device.name;
                    console.log('Connecting to ' + device.name);
                });
                sensorValues.deviceType = 'Glucose';
                BLE.connect(Settings.settings.glucose);
            }

        }, function (errorCode) {
            console.log('start scan failed');

        });
    };

    BLE.connect = function (connectToAddress) {
        console.log("Stopping scan, connecting to " + connectToAddress);

        var arrayLength = devices.length;
        var device;

        for (var i = 0; i < arrayLength; i++) {
            if (devices[i].address == connectToAddress) {
                device = devices[i];
                console.log("Found device");
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Found device: '+device.name;
                });
            }
        }

        device.connect(function (device) {
            console.log('Connected - reading services');
            if (sensorValues.deviceType == 'SpO2') {
                BLE.readServicesPulseOximeter(device);
            } else if (sensorValues.deviceType == 'Scale') {
                BLE.readServicesScale(device);
            } else if (sensorValues.deviceType == 'BP') {
                BLE.readServicesBP(device);
            } else if (sensorValues.deviceType == 'HRM') {
                BLE.readServicesHRM(device);
            } else if (sensorValues.deviceType == 'Glucose') {
                BLE.readServicesGlucose(device);
            }
            sensorMessage.message = 'Connected to '+device.name;
        }, function (errorCode) {
            $rootScope.$apply(function () {
                if (errorCode == 19) {
                    sensorMessage.message = 'Sensor disconnected';
                }
                else {
                    sensorMessage.message = 'Connect error '+errorCode;
                }
            });
            console.log('Connect error: ' + errorCode);
            BLE.startScan();
        });

    };

    BLE.readServicesBP = function (device) {
        myDevice = device;
        device.readServices(['00001810-0000-1000-8000-00805f9b34fb'
        ], function () {
            console.log('Services read');
            BLE.subscribeBP();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };

    /********* Blood Presure ****************/
    BLE.subscribeBP = function () {
        console.log('subscribing');

        // Set notification to ON.
        myDevice.writeDescriptor('00002a35-0000-1000-8000-00805f9b34fb', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([2, 0]), function () {
                console.log('writeDescriptor ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor error: ' + errorCode);
            });

        myDevice.enableNotification('00002a35-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('Notification:'+JSON.stringify(dataArray));
            $rootScope.$apply(function () {
                sensorValues.sys = dataArray[1];
                sensorValues.dia = dataArray[3];
                sensorValues.bp_mean = dataArray[5];
                sensorValues.sys_dia = sensorValues.sys+' / '+sensorValues.dia;
            });
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });

    };

    BLE.readBP = function (defer) {
        myDevice.readCharacteristic('00002a35-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('readScaleData'+JSON.stringify(dataArray));
        }, function (errorCode) {
            console.log('readScaleData error: ' + errorCode);
        });
    };

    /********* SCALE ****************/
    BLE.readServicesScale = function (device) {

        myDevice = device;
        device.readServices(['0000181d-0000-1000-8000-00805f9b34fb'
        ], function () {
            console.log('Services read');
            BLE.subscribeScale();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };

    BLE.subscribeScale = function () {
        console.log('subscribing');

        // Set notification to ON.
        myDevice.writeDescriptor('00002a9d-0000-1000-8000-00805f9b34fb', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([2, 0]), function () {
                console.log('writeDescriptor ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor error: ' + errorCode);
            });

        myDevice.enableNotification('00002a9d-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('Notification:'+JSON.stringify(dataArray));
            $rootScope.$apply(function () {
                sensorValues.kg = ((dataArray[2]*256)+dataArray[1])*5/1000;
                sensorValues.kgWithUnit = sensorValues.kg+' kg';
                console.log('sensorValues.kg='+sensorValues.kg);
            });
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });
    };


    BLE.readScaleData = function (defer) {
        myDevice.readCharacteristic('00002a9d-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('readScaleData'+JSON.stringify(dataArray));
        }, function (errorCode) {
            console.log('readScaleData error: ' + errorCode);
        });
    };


    /******* BLOOD SUGAR ****************/

    BLE.readServicesGlucose = function (device) {
        console.log('Reading glucose meter service');
        myDevice = device;
        device.readServices(['00001808-0000-1000-8000-00805f9b34fb'
        ], function () {
            console.log('Services read');
            BLE.subscribeGlucose();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };

    BLE.subscribeGlucose = function () {
        console.log('subscribing');

        // Set notification to ON.
        myDevice.writeDescriptor('00002a18-0000-1000-8000-00805f9b34fb', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([2, 0]), function () {
                console.log('writeDescriptor [2] ok');
                BLE.setGlucoseNotify();
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor error: ' + errorCode);
            });

        myDevice.enableNotification('00002a18-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            console.log('Notification:'+JSON.stringify(dataArray));
            sensorValues.glucoseDebug = JSON.stringify(dataArray);
            /*
            $rootScope.$apply(function () {
                sensorValues.sys = dataArray[1];
                sensorValues.dia = dataArray[3];
                sensorValues.bp_mean = dataArray[5];
                sensorValues.sys_dia = sensorValues.sys+' / '+sensorValues.dia;
                //sensorValues.pulse = dataArray[14];
                //sensorValues.pulseWithUnit = dataArray[14]+' bpm';
            });
            */
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });
    };

    BLE.setGlucoseNotify = function() {
        // Set notification to ON.
        myDevice.writeDescriptor('00002a18-0000-1000-8000-00805f9b34fb', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([1, 0]), function () {
                console.log('writeDescriptor [1] ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor error: ' + errorCode);
            });
    };

    /******* OXIMETER ****************/

    BLE.readServicesPulseOximeter = function (device) {

        myDevice = device;
        device.readServices(['46a970e0-0d5f-11e2-8b5e-0002a5d5c51b'
        ], function () {
            console.log('Services read');
            BLE.subscribePulseOximeter();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };

    // Subscribe summary data
    BLE.subscribePulseOximeter = function () {
        console.log('subscribing');

        // Set notification to ON.
        myDevice.writeDescriptor('0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([1, 0]), function () {
                console.log('writeDescriptor ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor da184025 error: ' + errorCode);
            });


        myDevice.enableNotification('0aad7ea0-0d60-11e2-8e3c-0002a5d5c51b', function (data) {
            var dataArray = new Uint8Array(data);
            //console.log('Notification:'+JSON.stringify(dataArray));
            if (dataArray[9] != 255 && dataArray[7] != 127) {
                $rootScope.$apply(function () {
                    sensorMessage.message = 'Received value from sensor';
                    sensorValues.pulse = dataArray[9];
                    sensorValues.SpO2 = dataArray[7];
                    sensorValues.pulseWithUnit = dataArray[9]+' bpm';
                    sensorValues.SpO2WithUnit = dataArray[7]+' %';
                });
            }
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });
    };

    /******* HRM Belt ****************/

    BLE.readServicesHRM = function (device) {

        myDevice = device;
        device.readServices(['0000180d-0000-1000-8000-00805f9b34fb'
        ], function () {
            console.log('HRM Services read');
            BLE.subscribeHRM();
        }, function (errorCode) {
            console.log('Error reading bleservices: ' + errorCode);
        });

    };

    // Subscribe summary data
    BLE.subscribeHRM = function () {
        console.log('subscribing HRM');

        // Set notification to ON.
        myDevice.writeDescriptor('00002a37-0000-1000-8000-00805f9b34fb', // Characteristic for data
            '00002902-0000-1000-8000-00805f9b34fb', // Configuration descriptor
            new Uint8Array([1, 0]), function () {
                console.log('writeDescriptor ok');
            }, function (errorCode) {
                // This error will happen on iOS, since this descriptor is not
                // listed when requesting descriptors. On iOS you are not allowed
                // to use the configuration descriptor explicitly. It should be
                // safe to ignore this error.
                console.log('writeDescriptor da184025 error: ' + errorCode);
            });

        myDevice.enableNotification('00002a37-0000-1000-8000-00805f9b34fb', function (data) {
            var dataArray = new Uint8Array(data);
            //console.log('Notification:'+JSON.stringify(dataArray));
            $rootScope.$apply(function () {
                // Todo: Should look at bit0 of first byte to see if int16 or int8
                sensorMessage.message = 'Received value from sensor';
                sensorValues.pulseHRM = dataArray[1];
                sensorValues.pulseHRMWithUnit = dataArray[1]+' bpm';
            });
        }, function (errorCode) {
            console.log('Notification error: ' + errorCode);
        });
    };

});

myAppServices.service('Settings', function ($rootScope) {
    var Settings = this;
    Settings.settings = {};

    Settings.getSettings = function() {
        if (Settings.settings.patientId == undefined) {
            console.log('Loading settings since they are undefined');
            Settings.loadSettings();
        }
        return Settings.settings;
    };

    Settings.getScope = function() {
        return Settings.scope;
    };

    Settings.saveSettings = function() {
        console.log('Saving settings: '+JSON.stringify(Settings.settings));
        localStorage.fhirSettings = JSON.stringify(Settings.settings);
    };

	/*
	 *   Specifik settings for devices and service and resource server
	 */
    Settings.loadSettings = function() {

        console.log('Loading settings');

        if (localStorage.fhirSettings == undefined) {
            Settings.settings.bpDevice = '5C:31:3E:01:81:48';
            Settings.settings.pulseOximetryDevice = '00:1C:05:FF:E1:16'; // nonin
            Settings.settings.scaleDevice = '5C:31:3E:01:18:A9';
            Settings.settings.pulseBelt = '00:22:D0:27:64:A6';
            Settings.settings.glucose = '00:60:19:27:4E:B0';            
            
            Settings.settings.FHIR_URL = 'http://fhir.ehelselab.com/hapi-fhir-jpaserver-mhelse/baseDstu2';
            Settings.settings.IDS_URL = 'https://oauth.ehelselab.com/connect/authorize'; 
			
			// Client specific		
			Settings.settings.patientId = '1'; // static
			Settings.settings.clientId = "implicitclient";			
            Settings.settings.redirectUrl = 'https://mhealthimplicitclient.azurewebsites.net/';
			Settings.settings.response_type = "token"; 
			Settings.settings.scope = "mobile.client";			
			Settings.settings.nonce = Math.random(); // No need to be cryptograhic random
			
			// Stronger random using pseudo-random generator in JavaScript crypto lib
			var PseudoRandArr = new Uint32Array(10);
			window.crypto.getRandomValues(PseudoRandArr);	
			Settings.settings.state = Date.now() + "" + PseudoRandArr.join('');
			localStorage["state"] = Settings.settings.state;

			Settings.settings.authUrl =
				Settings.settings.IDS_URL + "?" +
				"client_id=" + encodeURI(Settings.settings.clientId) + "&" +
				"redirect_uri=" + encodeURI(Settings.settings.redirectUrl) + "&" +
				"response_type=" + encodeURI(Settings.settings.response_type) + "&" +
				"scope=" + encodeURI(Settings.settings.scope) + "&" +
				"nonce="+ encodeURI(Settings.settings.nonce) + "&" +
				"state=" + encodeURI(Settings.settings.state);			
			
            Settings.saveSettings();
            //console.log('Wrote new FHIR-settings:'+JSON.stringify(Settings.settings));
        } else {
            Settings.settings = JSON.parse(localStorage.fhirSettings);
        }
        //console.log('Settings loaded:'+JSON.stringify(Settings.settings));
    };
});