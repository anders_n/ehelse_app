/**
 * Created by LarsKristian on 05.11.2015.
 * Edited by Anders Nordin on 01.02.2016
 */
angular.module('fhirApp.controllers', []).controller('LoginController', ['$scope', '$document','$location','$http', '$Settings', function($scope, $document, $location, $http, $httpProvider, $Settings) {

    $scope.accessToken = sessionStorage.getItem('fhirAccessToken');
    $scope.message = "Login and authorize this application to submit health data on your behalf.";

    $scope.saveToken = function() {
        console.log('Logging in...');
        $http.defaults.headers.common['Authorization']= 'Bearer '+$scope.accessToken;
        $scope.message = "Logged in";
        $location.path('/loggedin', false);
    };

	/*
	Constructing request link for sending to IdentityServer3
	*/
    $scope.makeLink = function () {
        sessionStorage.setItem("cordovaAppLocation", $location.absUrl());
        $scope.localUrl = $location.absUrl();

        var client_id = 'implicitclient';
        var redirect_uri = 'https://mhealthimplicitclient.azurewebsites.net/';
        var response_type = "token"; 
        var scope = "mobile.admin";
        var nonce = "6130755535249629";
        var state = Date.now() + "" + Math.random();
        localStorage["state"] = state;

        $scope.authUrl =
            AUTH_URL + "?" +
            "client_id=" + encodeURI(client_id) + "&" +
            "redirect_uri=" + encodeURI(redirect_uri) + "&" +
            "response_type=" + encodeURI(response_type) + "&" +
            "scope=" + encodeURI(scope) + "&" +
            "nonce="+ encodeURI(nonce) + "&" +
            "state=" + encodeURI(state);
    };

	/* 
	* Open inAppBrowser to send the request 
	*/
    $scope.loginInAppBrowser = function() {				
		var ref = cordova.InAppBrowser.open($scope.authUrl, '_blank', 'location=yes');
        ref.addEventListener('loadstart', function(event) { console.log(event.url);
            if (event.url.indexOf('access_token=') != -1) {
                ref.close();
                var accessToken = event.url.substring(event.url.indexOf('access_token=')+13);
                accessToken = accessToken.substr(0,accessToken.indexOf('&'));
                console.log('Received Access Token ' + accessToken);
                sessionStorage.setItem('fhirAccessToken', accessToken);
                $scope.$apply(function () {
                    $http.defaults.headers.common['Authorization']= 'Bearer '+accessToken;
                    $location.path('/loggedin', false);
                    $scope.accessToken = accessToken;
                });
            }
        });		
    };
	
    $document.ready(function(){
        $scope.makeLink();
    });
}]);